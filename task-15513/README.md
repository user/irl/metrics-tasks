Investigate lifetime of IPs on Hidden Services
==============================================

Extract hidden-service descriptors to descriptors/.

Obtain commons-codec-1.6.jar and place it in /usr/share/java/.

Compile and run the Java class:

  $ javac -d bin/ -cp /usr/share/java/commons-codec-1.6.jar \
    src/ParseDescriptors.java

  $ java -cp bin/:/usr/share/java/commons-codec-1.6.jar ParseDescriptors

Find verbose logs in lifetimes-verbose.txt.

Make sure you have R 3.0.2 with ggplot2 0.9.3.1 installed.

Plot graphs:

  $ R --slave -f plot.R

View output in Rplots.pdf


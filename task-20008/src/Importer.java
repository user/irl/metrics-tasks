/* Copyright 2016 The Tor Project
 * See LICENSE for licensing information */

import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Importer {

  public static void main(String[] args) throws Exception,
      SQLException {
    new Importer().importFiles(new File(args[0]));
  }

  private void importFiles(File directory) throws Exception,
      SQLException {
    this.connect();
    List<File> files = new ArrayList<>();
    files.add(directory);
    while (!files.isEmpty()) {
      File file = files.remove(0);
      if (file.isDirectory()) {
        files.addAll(Arrays.asList(file.listFiles()));
      } else {
        this.importFile(file);
      }
    }
    this.disconnect();
  }

  private Connection connection;

  PreparedStatement psFiles;

  PreparedStatement psResourcesSelect;

  PreparedStatement psResourcesInsert;

  PreparedStatement psRequests;

  void connect() throws SQLException {
    this.connection = DriverManager.getConnection(
        "jdbc:postgresql://localhost/webstats?user=vagrant&password=vagrant");
    this.connection.setAutoCommit(false);
    this.psFiles = this.connection.prepareStatement("INSERT INTO files "
        + "(server, site, log_date) VALUES (?, ?, ?)",
        Statement.RETURN_GENERATED_KEYS);
    this.psResourcesSelect = this.connection.prepareStatement(
        "SELECT resource_id FROM resources WHERE resource_string = ?");
    this.psResourcesInsert = this.connection.prepareStatement(
        "INSERT INTO resources (resource_string) VALUES (?)",
        Statement.RETURN_GENERATED_KEYS);
    this.psRequests = this.connection.prepareStatement("INSERT INTO requests "
        + "(file_id, method, resource_id, response_code, count,"
        + " total_bytes_sent) VALUES (?, CAST(? AS method), ?, ?, ?, ?)");
  }

  void disconnect() throws SQLException {
    this.connection.close();
  }

  final Pattern fileNamePattern =
      Pattern.compile("^(.+)-access.log-(\\d{8}).xz$");

  DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

  final Pattern logLinePattern = Pattern.compile("^0.0.0.[01] - - "
      + "\\[\\d{2}/\\w{3}/\\d{4}:00:00:00 \\+0000\\] "
      + "\"(GET|HEAD) ([^ ]+) HTTP[^ ]+\" (\\d+) (-|\\d+) \"-\" \"-\" -$");

  private void importFile(File file) throws Exception { // TODO catch exceptions!
    String server = file.getParentFile().getName();
    if (file.getName().contains("-ssl-access.log-")) {
      System.out.println("Skipping file: " + file.getAbsolutePath());
      return;
    }
    Matcher fileNameMatcher = this.fileNamePattern.matcher(file.getName());
    if (!fileNameMatcher.matches()) {
      System.err.println("Invalid file name: " + file.getAbsolutePath());
      return;
    }
    String site = fileNameMatcher.group(1);
    long logDateMillis = this.dateFormat.parse(fileNameMatcher.group(2))
        .getTime();
    int fileId = writeFile(server, site, logDateMillis);
    if (fileId < 0) {
      System.out.println("Skipping previously imported file: "
          + file.getAbsolutePath());
      this.connection.rollback();
      return;
    }
    BufferedReader br = new BufferedReader(new InputStreamReader(
        new XZCompressorInputStream(new FileInputStream(file))));
    String line;
    SortedMap<String, long[]> requests = new TreeMap<>();
    while ((line = br.readLine()) != null) {
      Matcher logLineMatcher = this.logLinePattern.matcher(line);
      if (!logLineMatcher.matches()) {
        System.err.println("Invalid line: " + line);
        this.connection.rollback(); // TODO reconsider
        br.close();
        return;
      }
      String method = logLineMatcher.group(1);
      String resource = this.truncateString(logLineMatcher.group(2), 2048);
      String responseCodeString = logLineMatcher.group(3);
      String combined = String.format("%s %s %s", method, resource,
          responseCodeString);
      long bytesSent = logLineMatcher.group(4).equals("-") ? 0L
          : Long.parseLong(logLineMatcher.group(4));
      long[] request = requests.get(combined);
      if (request == null) {
        request = new long[] { 1L, bytesSent };
      } else {
        request[0]++;
        request[1] += bytesSent;
      }
      requests.put(combined, request);
    }
    for (Map.Entry<String, long[]> request : requests.entrySet()) {
      String[] keyParts = request.getKey().split(" ");
      String method = keyParts[0];
      String resource = keyParts[1];
      int responseCode = Integer.parseInt(keyParts[2]);
      long[] valueParts = request.getValue();
      int count = (int) valueParts[0];
      long totalBytesSent = valueParts[1];
      this.writeRequest(fileId, method, resource, responseCode, count,
          totalBytesSent);
    }
    br.close();
    this.connection.commit();
    System.out.println("Successfully imported file: "
        + file.getAbsolutePath());
  }

  int writeFile(String server, String site, long logDateMillis) {
    int fileId = -1;
    try {
      this.psFiles.clearParameters();
      server = this.truncateString(server, 32);
      this.psFiles.setString(1, server);
      site = this.truncateString(site, 128);
      this.psFiles.setString(2, site);
      this.psFiles.setDate(3, new Date(logDateMillis));
      this.psFiles.execute();
      ResultSet resultSet = psFiles.getGeneratedKeys();
      if (resultSet.next()) {
        fileId = resultSet.getInt(1);
      }
      resultSet.close();
    } catch (SQLException e) {
      System.err.println("Could not insert row into files table: "
          + e.getMessage());
    }
    return fileId;
  }

  void writeRequest(int fileId, String method, String resource,
      int responseCode, int count, long totalBytesSent) throws Exception {
    int resourceId = -1;
    this.psResourcesSelect.clearParameters();
    this.psResourcesSelect.setString(1, resource);
    ResultSet rs = this.psResourcesSelect.executeQuery();
    if (rs.next()) {
      resourceId = rs.getInt(1);
    } else {
      /* There's a small potential for a race condition between the previous
       * SELECT and this INSERT INTO, but that will be resolved by the UNIQUE
       * constraint when committing the transaction. */
      this.psResourcesInsert.clearParameters();
      this.psResourcesInsert.setString(1, resource);
      this.psResourcesInsert.execute();
      ResultSet resultSet = psResourcesInsert.getGeneratedKeys();
      if (resultSet.next()) {
        resourceId = resultSet.getInt(1);
      } else {
        throw new Exception("Could not retrieve auto-generated key for new "
            + "resources entry."); // TODO better error handling
      }
      resultSet.close();
    }
    this.psRequests.clearParameters();
    this.psRequests.setInt(1, fileId);
    this.psRequests.setString(2, method);
    this.psRequests.setInt(3, resourceId);
    this.psRequests.setInt(4, responseCode);
    this.psRequests.setInt(5, count);
    this.psRequests.setLong(6, totalBytesSent);
    this.psRequests.execute();
  }

  private String truncateString(String originalString, int truncateAfter) {
    if (originalString.length() > truncateAfter) {
      System.err.println("String too long, truncating: " + originalString);
      originalString = originalString.substring(0, truncateAfter);
    }
    return originalString;
  }
}

